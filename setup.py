import logging
import os
import sys
import json
import datetime
import pandas as pd

### Parameters


def create_dir(path):
    """
    Create directory if path does not exist
    """
    
    if not os.path.exists(path):
        print("Created new directory: {0}".format(path))
        os.makedirs(path)
    else:
        print("Directory already exist: {0}".format(path))

    return

def create_file(project, project_path, file, file_format):
    """
    Create files from templates
    """
    
    source_path = "files/" + file
    #source_path = file
    ### Reading files
    with open(source_path, 'r') as b:
        if file_format == 'json':
            data = json.load(b)
        else:
            data = b.read()
    
    ### Replace necessary parameters
    replace_file_content = [
        "main.py", 
        "docker_script.txt", 
        "LICENSE.md", 
        "README.md"
    ]
    if file in replace_file_content:
        this_year = datetime.datetime.now().year
        data = data.replace("*YYYY*", str(this_year))
        data = data.replace("*XXXXXXXXXX*", project)
        if file == "docker_script.txt":
            cwd = os.getcwd()
            env_path = os.path.dirname(cwd)
            data = data.replace("*ENV_LOCATION*", env_path)
    
    ### Verify if file already exist, request if overwrite or not
    overwrite_bool = True
    if os.path.isfile(project_path+"/"+file):
        overwrite = input("{0}/{1} already exist. Do you want to overwrite? [Y/N]".format(project_path, file))
        overwrite = overwrite.upper()
        if ((overwrite == "YES") or (overwrite == "Y")):
            print("Overwrite - {0}: YES".format(project_path+"/"+file))
        else:
            print("Overwrite - {0}: NO".format(project_path+"/"+file))
            overwrite_bool = False
            #print("Exiting.")
            #sys.exit(0)

    ### Output
    if overwrite_bool:
        with open(project_path+"/"+file, 'w') as b:
            if file_format == 'json':
                json.dump(data, b,indent = 4)
            else:
                b.write(data)
            print("Created new file: "+project_path+"/"+file)

    return

def main():
    """
    Main execution script.
    """

    ### Getting the desired project name
    project = input("Please enter your project name: ")
    if project == "":
        print("No project name is entered.")
        print("Exiting.")
        sys.exit(0)
    print("Your project name: "+ project)
    
    ### PATH
    cwd = os.getcwd()
    test = os.path.dirname(cwd)
    
    ### Create Project Directory
    dir_path = "../"+project
    #dir_path = project
    create_dir(dir_path)
    create_dir(dir_path+"/src")

    ### Create Docker Environemtn Directory
    docker_env_path = "../kbc_data/"+project+"/data/"
    create_dir(docker_env_path+"in/files")
    create_dir(docker_env_path+"in/tables")
    create_dir(docker_env_path+"out/tables")
    create_dir(docker_env_path+"out/files")

    cwd = os.getcwd()
    test = os.path.dirname(cwd)

    ### Creating files
    create_file(project, dir_path+"/src", "main.py", "py")
    create_file(project, dir_path, "Dockerfile", "")
    create_file(project, docker_env_path, "docker_script.txt", "txt")
    create_file(project, docker_env_path, "config.json", "json")
    create_file(project, dir_path, "LICENSE.md", "md")
    create_file(project, dir_path, "README.md", "md")
    
    return


if __name__ == "__main__":

    main()

    print("Done.")