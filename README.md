# README #
This repository is a collection of functions which allows users to create a copy of python templates specifically designed for KBC custom science development.

### Configuration ###
User will be asked to enter the project name when the component runs. A new directory with the name of the project will be created in parallel of the location of this component folder. If same files are found during the setup.py process, the component will then ask the user whether or not if they want to replace the existing file. Therefore, it is user's responsibily to determine whether or not replacing the files.  

To modify the output of the template files, please apply adjustments into the files located inside the files folder, *NOT* the setup.py.

### Template's Description ###
1. main.py
    - The main script for custom science component's development
    - Destination: "../PROJECT_NAME/main.py"
2. Dockerfile
    - a script to setup the Docker environment with the necessary libraries
    - Destination: "../PROJECT_NAME/Dockerfile"
3. config.json
    - a configuration which can be used for local development
    - Due to the fact this file might contain sensitive information, it will be created in a separted folder (kbc_data) from the git repository folder
    - Destination: "..kbc\_data/PROJECT\_NAME/data/config.json"
4. docker_script.txt
    - a code to run the docker environment locally
    - Destination: "..kbc\_data/PROJECT\_NAME/data/docker_script.txt"
5. LICENSE.md
    - The MIT License
    - Destination: "../PROJECT_NAME/LICENSE.md"
6. README.md
    - Standard Readme template for custom component

### Contact Info ###

Leo Chan  
Vancouver, Canada (PST time)  
Email: leo@keboola.com  
Private: cleojanten@hotmail.com  
